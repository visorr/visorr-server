import { Schema } from 'mongoose';

export const LogSchema = new Schema(
  {
    details: {
      request: Schema.Types.Mixed,
      response: Schema.Types.Mixed,
      webPage: String,
    },
  },
  {
    timestamps: true,
  });
