import { Injectable } from '@nestjs/common';
import { CreateLogDto } from '../dtos/create-log.dto';
import { Log } from '../types/log.type';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class LogsService {
  constructor(@InjectModel('Log') private logModel: Model<Log>) {
  }

  async findAll(): Promise<Log[]> {
    return await this.logModel.find({}).exec();
  }

  async findById(logId: string): Promise<Log[]> {
    return await this.logModel.findById(logId).exec();
  }

  async create(dto: CreateLogDto): Promise<Log> {
    const createdLocation = new this.logModel({
      details: dto
    });
    return await createdLocation.save();

  }
}