export interface Log
  extends Readonly<{
    _id: string;
    details: {[key: string]: any},
    createdAt: string;
    updatedAt: string;
  }> {

}