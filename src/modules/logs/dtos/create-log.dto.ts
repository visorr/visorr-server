import {
  IsNotEmpty,
  IsString,
} from 'class-validator';

export class CreateLogDto {
  @IsNotEmpty()
  readonly request: any = null;

  @IsNotEmpty()
  readonly response: any = null;

  @IsNotEmpty()
  @IsString()
  readonly webPage: string = null;
}