import {
  Body,
  Controller,
  Get,
  Post,
  ValidationPipe,
  Param,
} from '@nestjs/common';
import { LogsService } from '../services/logs.service';
import { CreateLogDto } from '../dtos/create-log.dto';
import { Log } from '../types/log.type';

@Controller('logs')
export class LogsController {

  constructor(private logsService: LogsService) {
  }

  @Get()
  fetchAll(): Promise<Log[]> {
    return this.logsService.findAll();
  }

  @Get(':logId')
  fetchById(@Param('logId') logId: string): Promise<Log[]> {
    return this.logsService.findById(logId);
  }

  @Post()
  create(@Body(new ValidationPipe()) dto: CreateLogDto): Promise<Log> {
    return this.logsService.create(dto);
  }
}