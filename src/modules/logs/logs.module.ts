import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LogsController } from './controllers/logs.controller';
import { LogsService } from './services/logs.service';
import { LogSchema } from './log.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Log', schema: LogSchema}]),
  ],
  controllers: [LogsController],
  providers: [LogsService]
})
export class LogsModule {
}
